# Go IDE
## LiteIDE Install
> repo: https://github.com/visualfc/liteide

### [download]

- [Windows]
- [macOS]
- [Linux]


- [LiteIDE other download][]

- [Activity for liteide][]

### Install
#### windows
直接安装即可！

#### Linux
1. 解压安装
2. 源码编译安装
> [安装指导](https://github.com/visualfc/liteide/blob/master/liteidex/deploy/welcome/en/install.md)
> 编译安装较耗时(about 30 minutes)

##### Ubuntu 14.04

```
git clone https://github.com/visualfc/liteide.git
sudo apt-get update
sudo apt-get install qt4-dev-tools libqt4-dev libqt4-core libqt4-gui libqtwebkit-dev g++
cd liteide/build
./update_pkg.sh
QTDIR=/usr ./build_linux.sh

## Run it: ##
cd ~/liteide/build/liteide/bin
./liteide
```

##### Ubuntu 16.04
```
git clone https://github.com/visualfc/liteide.git
sudo apt-get update
sudo apt-get install qt4-dev-tools libqt4-dev libqtcore4 libqtgui4 libqtwebkit-dev g++
cd liteide/build
./update_pkg.sh
QTDIR=/usr ./build_linux.sh

## Run it: ##
cd ~/liteide/build/liteide/bin
./liteide
```

[download]: https://sourceforge.net/projects/liteide/?source=typ_redirect "sourceforge liteide"

[Windows]: https://downloads.sourceforge.net/project/liteide/X31/liteidex31.windows-qt4.zip
[macOS]: https://sourceforge.net/projects/liteide/files/X31/liteidex31.1.macosx-qt5.zip/download
[Linux]: https://sourceforge.net/projects/liteide/files/X31/liteidex31.linux64-qt4.tar.bz2/download

[LiteIDE other download]: http://www.golangtc.com/download/liteide "LiteIDE 其他下载地址"
[Activity for liteide]: https://sourceforge.net/p/liteide/activity/?page=0&limit=100#58c20afee88f3d42441206cd "sourceforge liteides"
