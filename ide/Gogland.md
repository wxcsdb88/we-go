# Go IDE

## Gogland Install
### [download][Gogland site]

- [Windows]
- [macOS]
- [Linux]

### IDE docs
[Getting Started with Gogland][gogland docs]

### Install
#### windows
Run the `gogland-171.3780.106.exe`, and configure the GOROOT and GOPATH, it will ok!

#### Linux
```
sudo wget https://download-cf.jetbrains.com/go/gogland-171.3780.106.tar.gz -c -P /opt
sudo tar -C /opt -xzf gogland-171.3780.106.tar.gz
```
##### start gogland
```
sudo /opt/Gogland-171.3780.106/bin/gogland.sh
```
And you can configure the desktop link when start!

### Go workspace should like
```
goWorkSpace  // (GOPATH)
  -- bin  // auto generat
  -- pkg  // .a files will store in
  -- src  // source code. go run, go install will work in
```

[Gogland site]: https://www.jetbrains.com/go/download/#section=windows
[Windows]: https://download-cf.jetbrains.com/go/gogland-171.3780.106.exe
[macOS]: https://download-cf.jetbrains.com/go/gogland-171.3780.106.dmg
[Linux]: https://download-cf.jetbrains.com/go/gogland-171.3780.106.tar.gz
[gogland docs]: https://www.jetbrains.com/help/go/1.0/getting-started-with-gogland.html
