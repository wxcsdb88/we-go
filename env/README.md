# Golang enviroment configure

## [download][go download site]

- [Microsoft Windows]
- [Apple OS X]
- [Linux]
- [Source][go sources]

### Stable versions
- go1.8
- go1.7.5
- go1.6.4

## Install and set
### win 10
1. Install go1.8.windows-amd64.msi
```
Install path c:\go
```
2. Set System Variables
```
GOROOT = C:\Go
path add the item %GOROOT%\bin
GOPATH = D:\work\golang
```

3. Check the install
```
go version
# output:
go version go1.8 windows/amd64
```

### linux or unix
1. download and decompress
```
sudo wget https://storage.googleapis.com/golang/go1.8.linux-amd64.tar.gz -c -P /opt
sudo tar -C /opt -xzf go1.8.linux-amd64.tar.gz
```
2. Set System Variables

  Add /opt/go/bin to the PATH environment variable. You can do this by adding this line to your /etc/profile(for a system-wide installation) or $HOME/.profile:
```
export GOROOT=/opt/go
export PATH=$GOROOT/bin:$PATH
export GOPATH=$HOME/company/golang
# export GOBIN=$GOPATH/bin # default is $GOPATH/bin
```
and then
```
source /etc/profile
```
or
```
source $HOME/.profile
```
3. Check the install
```
go version
# output:
go version go1.8 linux/amd64
```

### Go workspace should like
```
goWorkSpace  // (goWorkSpace为GOPATH目录)
  -- bin  // golang编译可执行文件存放路径，可自动生成。
  -- pkg  // golang编译的.a中间文件存放路径，可自动生成。
  -- src  // 源码路径。按照golang默认约定， go run， go install 等命令的当前工作路径（即在此路径下执行上述命令）。
```

[go download site]: https://golang.org/dl/
[Microsoft Windows]: https://storage.googleapis.com/golang/go1.8.windows-amd64.msi
[Apple OS X]: https://storage.googleapis.com/golang/go1.8.darwin-amd64.pkg
[Linux]: https://storage.googleapis.com/golang/go1.8.linux-amd64.tar.gz
[go sources]: https://storage.googleapis.com/golang/go1.8.src.tar.gz
